FROM python:slim-bookworm

RUN apt-get update && apt-get install -y apt-transport-https gnupg curl
RUN echo "deb https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN apt-get update && apt-get install -y kubectl google-cloud-sdk

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar los archivos de requerimientos de Python e instalar las dependencias
COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt


RUN  apt-get install -y google-cloud-sdk-gke-gcloud-auth-plugin

# Copiar tu código fuente al directorio /app dentro del contenedor
COPY *.py /app
