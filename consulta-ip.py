import requests
import datetime


def obtener_direccion_ip():
    try:
        # Realizar la solicitud HTTP para obtener la dirección IP
        response = requests.get("http://icanhazip.com/")
        if response.status_code == 200:
            return response.text.strip()
        else:
            return "Error: No se pudo obtener la dirección IP"
    except Exception as e:
        return f"Error: {str(e)}"


def registrar_linea_de_log(direccion):
    try:
        # Obtener la fecha y hora actual
        fecha_hora_actual = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Crear una línea de registro con la fecha, hora y dirección IP
        linea_de_log = f"{fecha_hora_actual} - Dirección IP: {direccion}\n"

        # Abrir el archivo de registro en modo apendice y escribir la línea de registro
        with open("registro.txt", "a") as archivo:
            archivo.write(linea_de_log)
    except Exception as e:
        print(f"Error al registrar la línea de log: {str(e)}")


if __name__ == "__main__":
    ip = obtener_direccion_ip()
    registrar_linea_de_log(ip)
    print(f"La dirección IP actual es: {ip}")
